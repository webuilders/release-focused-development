# Estimating project calculation (draft)

 0. **Estimation categories**
    + Entities (nouns - stored in DB with CRUD operations)
    + Actions (verbs - business procedures)
    + Views (GUI screens, requires coding and sometimes components)